export class User {
  public cid : number;
  public pseudo: String;
  public mail: String;
	public password: String;
	public role: String;
	public phone: String;
	public titre: String;
	public adresse: String;
	public description: String;
	public papierVerifier: String;
	public estverifier: String;
	public cin: number;
	public nom: String;
	public prenom: String;
	public dateNaissance: Date;
	public gouvernorat: String;
	public photoId: String;
  constructor(cid : number, mail: String,password: String,role: String, phone: String,){
    this.mail=mail;
    this.cid=cid;
    this.password=password;
    this.role=role;
    this.phone=phone;

  }

}

