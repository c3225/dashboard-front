export class Donateur {
  public cid : number;
  public adresse: String;
  public mail: String;
	public password: String;
	public role: String;
	public cin: number;
	public nom: String;
	public prenom: String;
	public dateNaissance: Date;
	public phone: String;
	public gouvernorat: String;
	public photoLink: String;
  constructor(cid : number,mail: String,password: String,role: String,cin: number,nom: String,prenom: String,dateNaissance: Date,phone: String,gouvernorat: String,adresse: String,photoLink: String){
    this.mail=mail;
    this.cid=cid;
    this.password=password;
    this.role=role;
    this.cin=cin;
    this.nom=nom;
    this.prenom=prenom;
    this.dateNaissance=dateNaissance;
    this.phone=phone;
    this.gouvernorat=gouvernorat;
    this.adresse=adresse;
    this.photoLink=photoLink;

  }
}
