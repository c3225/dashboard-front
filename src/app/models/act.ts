export class Act {
    public id : number;
    public titre: String;
    public description: String;
    public estlivre: boolean;
    public associationId: number;
    public photosUrl : String;
    constructor(id:number,titre:String,description: String,estlivre: boolean,associationId: number,photosUrl:String){
      this.titre=titre;
      this.description=description;
      this.estlivre=estlivre;
      this.associationId=associationId;
      this.photosUrl=photosUrl;

    }


}
