import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { AssociationService } from 'src/app/services/association.service';
import { UserService } from 'src/app/services/user.service';
import { UpdateModelComponent } from '../update-model/update-model.component';

@Component({
  selector: 'app-association',
  templateUrl: './association.component.html',
  styleUrls: ['./association.component.scss']
})
export class AssociationComponent implements OnInit {
  //lesassociations : User[];
  users: any;
  constructor(public as:AssociationService, public dialog: MatDialog,public us: UserService) {

    this.as.getTousAssociations().subscribe(
      data =>{this.users=data;},
      error=>{alert("affiche impo")}
    );

   }
   ondelete(cid:number){
    if(confirm('are you sure to delete this user')){
      this.us.supprimerUser(cid).subscribe(
        data =>{this.users=data;},
      error=>{alert("delete impo")}
      );
    }
  }
   updateU(user:any){
    const dialogref= this.dialog.open(UpdateModelComponent, {width:'500px',height:'600px',data: user});
    dialogref.afterClosed().subscribe(
      res=>{this.as.getTousAssociations().subscribe((data)=>{this.users=data})});
      this.ngOnInit();
  }

  ngOnInit(): void {
  }



}
