import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-button-add',
  templateUrl: './button-add.component.html',
  styleUrls: ['./button-add.component.scss']
})
export class ButtonAddComponent implements OnInit {
users: any;
  constructor(private as:UserService,public dialog:MatDialogRef<ButtonAddComponent>,@Inject(MAT_DIALOG_DATA) public data:any) {}

  AddMethode (f:NgForm)
  {
    let cid = f.value["cid"] ;
    let role = f.value["role"];
    let mail =f.value["mail"];
    let phone =f.value["phone"];
    let password =f.value["password"];

    let newusers  = new User(cid,mail,password,role,phone) ;
    this.as.ajoutUser(newusers).subscribe(
      (User)=>
      {this.users =[User, this.users] ;

      },
      
      error => {

        console.log(error);
    }

      )
      this.dialog.close();

  }
  closee(){
    this.dialog.close();
  }

  ngOnInit(): void {
    // this.as.getTousUsers().subscribe(
    //   data =>{this.users=data;},
    //   error=>{alert("affiche impo")}
    // );

  }

}
