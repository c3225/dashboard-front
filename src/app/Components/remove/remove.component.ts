import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-remove',
  templateUrl: './remove.component.html',
  styleUrls: ['./remove.component.scss']
})
export class RemoveComponent implements OnInit {
cid : number;
  constructor(public su : UserService, public ar : ActivatedRoute) {
    this.ar.params.subscribe(
      data => {this.cid = data.cid; }

    );
  }
  valider(){
    this.su.supprimerUser(this.cid).subscribe(
      data=>{alert("utilisateur supprimé");this.cid=0;},
      error=>{alert("Erreur de supprision");}
    )}

annuler(){
  alert("cancel");
}
  ngOnInit(): void {
  }

}
