import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { DonateurService } from 'src/app/services/donateur.service';
import { UserService } from 'src/app/services/user.service';
import { UpdateModelComponent } from '../update-model/update-model.component';

@Component({
  selector: 'app-donateur',
  templateUrl: './donateur.component.html',
  styleUrls: ['./donateur.component.scss']
})
export class DonateurComponent implements OnInit {
  //lesdonateurs : User[];
  users: any;
  constructor(public as:DonateurService,private dialog: MatDialog,public us:UserService) {

    this.as.getTousDonateurs().subscribe(
      data =>{this.users=data;},
      error=>{alert("affiche impo")}
    );

   }
   ondelete(cid:number){
    if(confirm('are you sure to delete this user')){
      this.us.supprimerUser(cid).subscribe(
        data =>{this.users=data;},
      error=>{alert("delete impo")}
      );
      this.ngOnInit();
    }
    this.ngOnInit();
  }
  updateU(user:any){
    const dialogref= this.dialog.open(UpdateModelComponent, {width:'500px',height:'600px',data: user});
    dialogref.afterClosed().subscribe(
      res=>{this.as.getTousDonateurs().subscribe((data)=>{this.users=data})});
      this.ngOnInit();
  }
  ngOnInit(): void {
  }



}
