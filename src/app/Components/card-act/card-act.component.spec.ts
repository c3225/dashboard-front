import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardActComponent } from './card-act.component';

describe('CardActComponent', () => {
  let component: CardActComponent;
  let fixture: ComponentFixture<CardActComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardActComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardActComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
