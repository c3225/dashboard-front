import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActService } from 'src/app/services/act.service';

@Component({
  selector: 'app-card-act',
  templateUrl: './card-act.component.html',
  styleUrls: ['./card-act.component.scss']
})
export class CardActComponent implements OnInit {
actes:any;
  constructor(public http :HttpClient,public as: ActService) {
    this.as.getTousActes().subscribe(
      data =>{this.actes=data;},
      error=>{alert("affiche impossible")}
    );
  }

  ngOnInit(): void {
  }

}
