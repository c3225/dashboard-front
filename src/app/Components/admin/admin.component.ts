import { AdminService } from '../../services/admin.service';
import { Admin } from '../../models/admin';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  lesadmins : Admin[];
  constructor(public as:AdminService) {
    this.as.getTousAdmins().subscribe(
      data =>{this.lesadmins= data;},
      error=>{alert("affiche impo")}
    );
   }

  ngOnInit(): void {
  }

}
