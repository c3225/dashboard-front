import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { UpdateModelComponent } from '../update-model/update-model.component';
import { ButtonAddComponent } from '../button-add/button-add.component';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
//lesusers : User[];
//user: any;
users: any;
res:  any;
apiUri="http://localhost:8484/CharityLandRest/rest/users";
  constructor(public as:UserService,public http :HttpClient,public dialog: MatDialog) {

    this.as.getTousUsers().subscribe(
      data =>{this.users=data;},
      error=>{alert("affiche impo")}
    );

   }

   add (f:NgForm)
   {
    let cid=f.value["cid"];
   let phone = f.value["phone"];
   let mail = f.value["mail"];
   let password = f.value["password"];
   let role = f.value["role"];
   let user= new User(cid, mail,password,role, phone);
         this.as.ajoutUser(user).subscribe(
           (User)=> {this.users = [user, this.users];}
           );


   this.ngOnInit();

     }

   ondelete(cid:number){
    if(confirm('are you sure to delete this user')){
      this.as.supprimerUser(cid).subscribe(
        data =>{this.users=data;},
      error=>{alert("delete impo")}
      );
      this.ngOnInit();
    }
    this.ngOnInit();
  }
  addU(){
    const dialogref= this.dialog.open(ButtonAddComponent, {width:'500px',height:'600px'});
    dialogref.afterClosed().subscribe(
      res=>{this.http.get(this.apiUri).subscribe((data)=>{this.users=data})});
      this.ngOnInit();
  }
  updateU(user:any){
    const dialogref= this.dialog.open(UpdateModelComponent, {width:'500px',height:'600px',data: user});
    dialogref.afterClosed().subscribe(
      res=>{this.http.get(this.apiUri).subscribe((data)=>{this.users=data})});
      this.ngOnInit();
  }


  title = 'admin-panel-layout';
  sideBarOpen = false;

  sideBarToggler() {
    this.sideBarOpen = !this.sideBarOpen;
  }
  ngOnInit(): void {
    this.as.getTousUsers().subscribe(
      data =>{this.users=data;},
      error=>{alert("affiche impo")}
    );

  }



}
