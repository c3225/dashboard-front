import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActService } from 'src/app/services/act.service';

@Component({
  selector: 'app-act',
  templateUrl: './act.component.html',
  styleUrls: ['./act.component.scss']
})
export class ActComponent implements OnInit {
  actes:any;
  constructor(public http :HttpClient,public as: ActService) {
    this.as.getTousActes().subscribe(
      data =>{this.actes=data;},
      error=>{alert("affiche impossible")}
    );
  }
  ondelete(id:number){
    if(confirm('are you sure to delete this acte')){
      this.as.supprimerActe(id).subscribe(
        data =>{this.actes=data;},
      error=>{}
      );
      this.ngOnInit();
    }
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.as.getTousActes().subscribe(
      data =>{this.actes=data;},
      error=>{alert("affiche impossible")}
    );
  }

}
