import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-model',
  templateUrl: './update-model.component.html',
  styleUrls: ['./update-model.component.scss']
})
export class UpdateModelComponent implements OnInit {

  constructor(private http: HttpClient, private as:UserService,public dialog:MatDialogRef<UpdateModelComponent>,@Inject(MAT_DIALOG_DATA) public data:any) { }

  UpdateMethode (f:NgForm)
  {
    let cid = this.data.cid ;
    let role = f.value["role"];
    let mail =f.value["mail"];
    let phone =f.value["phone"];
    let password =f.value["password"];


    let users  = new User(cid,mail,password,role,phone) ;
    let x = {cid:cid, ...users} ;
    this.as.modifierUser(x).subscribe(
      ()=>
      {
        this.dialog.close();
      },
      error => {

        console.log(error);
    }

      )

  }
  closee(){
    this.dialog.close();
  }
  ngOnInit(): void {
  }

}
