import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { ActComponent } from './Components/act/act.component';
import { MatDialogModule } from '@angular/material/dialog';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './Components/user/user.component';
import { ButtonAddComponent } from './Components/button-add/button-add.component';
import { RemoveComponent } from './Components/remove/remove.component';
import { DonateurComponent } from './Components/donateur/donateur.component';
import { AssociationComponent } from './Components/association/association.component';
import { UpdateModelComponent } from './Components/update-model/update-model.component';
import { CardActComponent } from './Components/card-act/card-act.component';
import {MatCardModule} from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    DashboardComponent,
    SidenavComponent,
    ActComponent,
    LoginAdminComponent,
    UserComponent,
    ButtonAddComponent,
    RemoveComponent,
    DonateurComponent,
    AssociationComponent,
    UpdateModelComponent,
    CardActComponent,
  ],
  entryComponents: [
    ButtonAddComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    BrowserAnimationsModule,
    // * MATERIAL IMPORTS
    MatSidenavModule,
    MatToolbarModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule


  ],
  providers: [FormBuilder,LoginAdminComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
