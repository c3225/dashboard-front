import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Admin } from '../models/admin';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  apiUrl="http://localhost:8484/apiCharityRest/rest/users/admin";

  constructor(public httpadmin :HttpClient) { }
  getTousAdmins()
 {
   return this.httpadmin.get<Admin[]>(this.apiUrl);
 }
 ajoutadmin(a:Admin)
  {
     return this.httpadmin.post<any>(this.apiUrl, a);
  }

  supprimeradmin(id:number)
  {
    return this.httpadmin.delete<any>(this.apiUrl+"/"+ id);
  }

  getadmin(id:number)
  {
    return this.httpadmin.get<Admin>(this.apiUrl+"/"+id);
  }

  modifieradmin(a:Admin)
  {
    return this.httpadmin.put(this.apiUrl, a);
  }
}
