import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AssociationService {
  apiUrl="http://localhost:8484/CharityLandRest/rest/users/association";
  constructor(public httpassociation : HttpClient) { }
 getTousAssociations()
 {
   return this.httpassociation.get<User[]>(this.apiUrl);
 }
 ajoutAssociation(d:User)
  {
     return this.httpassociation.post<any>(this.apiUrl, d);
  }

  supprimerAssociation(id:number)
  {
    return this.httpassociation.delete<any>(this.apiUrl+"/"+ id);
  }

  getAssociation(id:number)
  {
    return this.httpassociation.get<User>(this.apiUrl+"/"+id);
  }

  modifierAssociation(p:User)
  {
    return this.httpassociation.put(this.apiUrl, p);
  }
}
