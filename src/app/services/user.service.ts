import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl="http://localhost:8484/CharityLandRest/rest/users";
  constructor(private httpuser :HttpClient) { }
  getTousUsers()
 {
   return this.httpuser.get<User[]>(this.apiUrl);
 }
 ajoutUser(a:User)
  {
     return this.httpuser.post<any>(this.apiUrl, a);
  }

  supprimerUser(cid:number)
  {
    return this.httpuser.delete<User[]>(this.apiUrl+"/"+ cid);
  }

  getUser(cid:number)
  {
    return this.httpuser.get<User>(this.apiUrl+"/"+cid);
  }

  modifierUser(u:User)
  {
    return this.httpuser.put(this.apiUrl, u);
  }
}

