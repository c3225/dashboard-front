import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Act } from '../models/act';

@Injectable({
  providedIn: 'root'
})
export class ActService {
  apiUrl="http://localhost:8484/CharityLandRest/rest/actes";
  constructor(private http :HttpClient) { }
  getTousActes()
 {
   return this.http.get<Act[]>(this.apiUrl);
 }
 ajoutActe(a:Act)
  {
     return this.http.post<any>(this.apiUrl, a);
  }

  supprimerActe(id:number)
  {
    return this.http.delete<Act[]>(this.apiUrl+"/"+ id);
  }

  getActe(id:number)
  {
    return this.http.get<Act>(this.apiUrl+"/"+id);
  }

  modifierActe(a:Act)
  {
    return this.http.put(this.apiUrl, a);
  }
}
