import { Donateur } from './../models/donateur';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class DonateurService {
  apiUrl="http://localhost:8484/CharityLandRest/rest/donateur";
  constructor(public httpdonateur : HttpClient) { }
 getTousDonateurs()
 {
   return this.httpdonateur.get<User[]>(this.apiUrl);
 }
 ajoutDonateur(d:User)
  {
     return this.httpdonateur.post<any>(this.apiUrl, d);
  }

  supprimerDonateur(id:number)
  {
    return this.httpdonateur.delete<any>(this.apiUrl+"/"+ id);
  }

  getDonateur(id:number)
  {
    return this.httpdonateur.get<User>(this.apiUrl+"/"+id);
  }

  modifierDonateur(p:User)
  {
    return this.httpdonateur.put(this.apiUrl, p);
  }
}
