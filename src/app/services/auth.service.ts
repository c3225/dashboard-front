import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { User } from '../models/user';
@Injectable({ providedIn: 'root' })
export class AuthService {
  public API_URL = "http://localhost:8484/CharityLandRest/rest/users/login"
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public loginUser: String;
  public logged:String;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    //this.loginUser=JSON.parse(localStorage.getItem('currentUser')).mail;
      this.logged=JSON.parse(localStorage.getItem('currentUser'));
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(mail: string, password: string) {
    let body = new URLSearchParams();
    body.set('mail', mail);
    body.set('password', password);

    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };


    return this.http.post(this.API_URL, body.toString(), options)
    .pipe(map(user => {
      // store user details jwt token in localStorage
      localStorage.setItem('currentUser', JSON.stringify(user));
      console.log("cc1"+user);
this.loginUser=JSON.parse(localStorage.getItem('currentUser')).mail;
      console.log("hhhh"+this.loginUser);
      return user;
    }));

  }
  islogged(): Boolean{
    if(this.logged==""){
       return false;
    }

    return true;
  }

  logout() {
    //remove user from localStorage
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
