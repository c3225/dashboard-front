import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { ActComponent } from './Components/act/act.component';
import { UserComponent } from './Components/user/user.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { ButtonAddComponent } from './Components/button-add/button-add.component';
import { RemoveComponent } from './Components/remove/remove.component';
import { DonateurComponent } from './Components/donateur/donateur.component';
import { AssociationComponent } from './Components/association/association.component';
import { UsersGuard } from './users.guard';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', component: LoginAdminComponent },

  { path: 'dashboard', component: DashboardComponent },
  { path: 'act', component: ActComponent },
  { path: 'user', component: UserComponent },
  { path: 'association', component: AssociationComponent },

  { path: 'donateur', component: DonateurComponent },

  { path: 'login', component: LoginAdminComponent },
  { path: 'adduser', component: ButtonAddComponent },
  { path: 'remove/:cid', component: RemoveComponent },






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
